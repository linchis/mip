package com.android.mip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import com.android.mip.controlador.Controlador;
import com.android.mip.utilidades.Utilidades;


public class Splash extends Activity{
	 	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        //guardar el contexto de aplicacion
        Controlador.contextoAPP = getApplicationContext();               
	}
	
	
	
	@Override
	protected void onResume() {	
		super.onResume();
			      
	        if (Controlador.isOnlineApp()){
	        	
	        	String ipv4 = Utilidades.getIPAddress(true);
	        	((TextView)findViewById(R.id.ip)).setText(ipv4);
		        
	        }else{
	        	new AlertDialog.Builder(Splash.this)
		      	.setIcon(android.R.drawable.ic_dialog_alert)
		      	.setTitle("AVISO")
		      	.setMessage("No conectado a Internet")   	
		      	.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {//un listener que al pulsar, cierre la aplicacion
		      		@Override
			        public void onClick(DialogInterface dialog, int which){
		      			Splash.this.finish();
			        }
		      	})
		      	.show();
	        }                
    }
	
	
}
