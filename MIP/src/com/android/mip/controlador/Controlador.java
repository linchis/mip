package com.android.mip.controlador;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Controlador {

	
	public Controlador(){}
	
	
	public static Context contextoAPP;
	
	
		
	
	//Metodos
	public static boolean isOnlineApp() {
	    ConnectivityManager cm = (ConnectivityManager) contextoAPP.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();	    
	    if (netInfo != null && netInfo.isConnected()) {	    
	        return true;
	    }
	    return false;
	}
	
	
	//Extrae el valor del parametro indicado en una URL
	public static String urlGetParamValue(String url, String name){
  		String valor = "";
  			String[] partes = url.split("\\?");
  			String[] params = partes[1].split("\\&");
  		
  			for (int i=0;i<params.length;i++){
  				if (params[i].contains(name)){
  					valor = params[i].split("=")[1]; 
  				}
  			}  			  		
  		return valor;  		
  	}
	
}
